import { Router } from "express";
import UserContoller from "../controllers/userController";

const router = Router();

router.get("/", UserContoller.getAllUsers);
router.get("/:id", UserContoller.getUserById);
router.post("/", UserContoller.addUser);

export default router;
