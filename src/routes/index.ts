import express from "express";
import UserRouter from "./userRouter";

export const Routes = (app: express.Application) => {
  app.use("/api/users", UserRouter);
};
