import { PrismaClient, Users, Roles, Prisma } from "../db";
const prisma = new PrismaClient();

type UsersAndRoles = (Users & Roles) | null;

export default new (class UserModel {
  async addUser(userInput: Prisma.UsersCreateInput): Promise<Users> {
    const user = await prisma.users.create({
      data: userInput,
    });
    return user;
  }
  async getAllUsers(): Promise<Users[]> {
    const user = await prisma.users.findMany({});
    return user;
  }
  async getUserById(id: number): Promise<UsersAndRoles> {
    const user = await prisma.users.findFirst({
      where: {
        id,
      },
      include: {
        role: true,
      },
    });
    return user;
  }
})();
