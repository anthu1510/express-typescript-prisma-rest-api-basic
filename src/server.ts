import dotenv from "dotenv";
import http from "http";
import App from "./start/app";

dotenv.config();
const server = http.createServer(App);

server.listen(process.env.PORT, () =>
  console.log(`server stated at http://${process.env.HOST}:${process.env.PORT}`)
);
