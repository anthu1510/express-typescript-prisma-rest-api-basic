import { Request, Response } from "express";
import UserModel from "../models/userModel";

class UserContoller {
  async getUserById(req: Request, res: Response): Promise<void> {
    const users = await UserModel.getUserById(parseInt(req.params.id));
    res.send(users);
  }
  async getAllUsers(req: Request, res: Response): Promise<void> {
    const users = await UserModel.getAllUsers();
    res.send(users);
  }
  async addUser(req: Request, res: Response): Promise<void> {
    const user = await UserModel.addUser(req.body);
    res.send(user);
  }
}

export default new UserContoller();
