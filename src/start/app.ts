import express from "express";
import { Routes } from "../routes";

const app: express.Application = express();

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

Routes(app);

export default app;
